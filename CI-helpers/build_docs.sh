#!/usr/bin/env bash
######################################################################
# Documentation build script for CI-pipeline
#
# Initially written by Marko Kosunen, marko .kosunen@aalto.fi, 16.4.2022
######################################################################

#Function to display help with -h argument and to control
#the configuration from the command line
help_f()
{
SCRIPTNAME="build_docs.sh"
cat << EOF
${SCRIPTNAME} Release 1.0 (16.4.2022)

Written by Marko Kosunen

SYNOPSIS
    $(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:]) [OPTIONS]
DESCRIPTION
  Compiles docstrings documentation for a mosaic_BAG module that has a proper 
  documentation sphinx-build Makefile strored in a directory 'doc'.

OPTIONS
  -c
      If given, will assume that the script is executed as part of CI 
      process.
  -e
      Export local variables to environment variables
  -m  
      Module directory. Default <current directory>/doc
  -v
      Virtuoso directory. Default current directory
  -h
      Show this help.
EOF
}


# We assume that this script is called from the root of the chopndale_template
PROJ_DIR="$(pwd)"
MODULEDIR="$(pwd)"
EXPORT='0'
ISCI='0'

while getopts cem:v:h opt
do
  case "$opt" in
    c) ISCI='1';;
    e) EXPORT='1';;
    m) MODULEDIR=${OPTARG};;
    v) VIRTUOSO_DIR=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

PATH=${PATH}:${HOME}/.local/bin
# Set here all the needed variables to build the documentations


if [ "${EXPORT}" == '1' ] || [ "${ISCI}" == '1' ]  ; then
    #Here export those variables if you wish to have them as environmetn variables
    echo "" > /dev/null
fi

if [ "${ISCI}" == '1' ]  ; then
    ${PROJ_DIR}/CI-helpers/clone-recursive-https.sh ${PROJ_DIR}
    if [ -z "${CI_PROJECT_DIR}" ]; then
        echo "CI_PROJECT_DIR not defined. Please set it as an environment variable"
        exit 1
    fi
else
    CI_PROJECT_DIR="${PROJ_DIR}"
fi
${PROJ_DIR}/init_submodules.sh

rm -rf $CI_PROJECT_DIR/public
cd ${MODULEDIR}/doc/ && ./configure && make html && mv ./build/html $CI_PROJECT_DIR/public || $(echo "Build failed"; exit 1)

exit 0



