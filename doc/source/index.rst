.. thesdk documentation master file.
   You can adapt this file completely to your liking, 
   but it should at least contain the root `toctree` directive.

===============================
Chip 'n Dale SoC flow template
===============================

..  toctree::
    :maxdepth: 3
    :caption: Contents:

    introduction

    user_guide/user_guide

    flow_configuration/flow_configuration

    documentation_instructions

    developers_guide

    indices_and_tables
   



