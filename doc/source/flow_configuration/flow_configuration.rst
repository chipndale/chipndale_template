.. flow configurations documentation master file.
   You can adapt this file completely to your liking, 
   but it should at least contain the root `toctree` directive.

==================
Flow configuration
==================

The instructions for the flow documentation are presented in the way that  
supposedly supports well your efforts to define the required parameters for  
the flow. Remember that most often we are working under the strict requirements
of semiconductor process NDAs, it is impossible to provide exact values but
merely hints of how to define the value.

Master source of the documentation are the markdown issues found at
`https://gitlab.com/chipndale/process_independent_analog_shell/-/tree/master/process_setup_manager <https://gitlab.com/chipndale/process_independent_analog_shell/-/tree/master/process_setup_manager>`_

Flow configuration using gitlab issues
--------------------------------------
For this setup you need at least gitlab community edition installed.

The most consistent way to perform the flow configuration is to use  
`https://gitlab.com/chipndale/process_independent_analog_shell/-/blob/master/process_setup_manager/generate_setup_process.py?ref_type=heads <generate_setup_process.py>`_ 
delivered at `environment_setup/process_independent_analog_shell/process_setup_manag/generate_setup_process.py`.  
To perform the setups, you need to: first perform the following tasks:  

  1. Create a **process specific** gitlab group under which you will place the local forks of the template projects.
  This group is a process specific group that will point automatically to correct process specific submodules maintained by **your organization**.
  Hereafter we refer to this as :code:`<PROCESS_GROUP>`

  2. Fork the following projects from `https://gitlab.com/chipndale <https://gitlab.com/chipndale>`_

       1. `https://gitlab.com/chipndale/chipndale_template <https://gitlab.com/chipndale/chipndale_template>` to project :code:`<proces_identifier>_chip`

       2. `https://gitlab.com/chipndale/dale_innocus_21.1 <https://gitlab.com/chipndale/dale_innovus_21.1>`_ . Change the :code:`spi_slave` 
       submodule under your `<proces_identifies>_chip` to point to this fork.

       3. `https://gitlab.com/chipndale/process_dependent_configs_and_helpers <https://gitlab.com/chipndale/process_dependent_configs_and_helpers>`_. 
       Change the `process_dependent_configs_and_helpers` under your :code:`spi_slave` submodule to point to this fork.

       4. `https://gitlab.com/chipndale/macro_repository <https://gitlab.com/chipndale/macro_repository>`_. Change the :code:`macro_repository` submodule of 
       `<proces_identifies>_chip` to point to this fork.

       5. `https://gitlab.com/chipndale/environment_setup_template <https://gitlab.com/chipndale/environment_setup_template>`_. Change the :code:`environment_setup`
       submodule of `<proces_identifier>_chip` to point to this fork.


Then perform the following tasks 
  1. Clone the project :code:`<proces_identifier>_chip`  
  2. :code:`cd <proces_identifier>_chip && ./init submodules.sh`  
  3. :code:`cd environment_setup/process_independent_analog_shell/process_setup_manager`  
  4. :code:`./generate_setup_process.py --url=<URL of your gitlab server> --token=<your access token to the server> --group=<PROCESS_GROUP> --users=\"<your username at the sever>\"` Add :code:`--no-ssl` if needed, and set environment variable :code:`ACCESSTOKEN` to your access token if needed.

This should create number of issues under :code:`environment_setup` project under yous :code:`<PROCESS_GROUP>`. 
Then just close the issues and you are done :) .  Isssues are listed below in case you fail to create them, but still would like to have some idea what you should do.

.. toctree::
   :glob:
   :maxdepth: 3
   :caption: Issues in intended order of completion

   Issue-*

