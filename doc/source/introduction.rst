=============================================
Introduction to Chip 'n' Dale SoC build flow
=============================================

Background
==========

Chip'nDale is a complete tape-out flow for mixed-mode microelectronics 
circuits

To understand the flow, it is essential to uderstand the dependencies 
between the different conceptual parts of the flow. The parts and how to use
them are documented in :doc:`user_guide/user_guide`. Brief description of the
parts is provided below.

Environment setup
-----------------

Environmetn setup provided and managed as a git submodule that provides all the
necessary configuration-, setup-, and helper scripts for the chip compilatio
process. 

Macro release
-------------
The flow builds strongly on concept of *macro release*, provided as shell script
``environment_setup/process_independent_analog_shell/macro_release.sh``. You
can do analog macro releases once you have set up your virtuoso environment,
and releases with files supporting the digital flow after you have configured the file
``environment_setup/lefgen_files/gen_abstract_replay.sh``.

Macro release concept is essentia for this flow. It is use to store the
Virtuoso designs to Git version control so that they can be retrieved from
there and utilized by the flow. 


Digital subflows (the Dale-part)
--------------------------------
Any digital subdesign can be included in the flow as a git submodule folowing
the excample structure of ``spi_slave`` submodule.


Chip'nDale Chip flow
====================
Chip flow merges the designs provided either by macro releses or digital
subdesing flow an executes the sign-off procedures on the toplevel design. 



TL;DR 
-----

Once you have the tools and proces configurations in place, this is how the flow works  

```./init_submodules.sh  

source sourceme.sh and follow the instructions (literally)  

./configure && make  

make debug_virtuoso ``` 

For help, 

make help

The fundamental rule
....................
The chip is build with the following sequence:
* ./init_submodules.sh (versioning)
* source sourceme.sh (tools)
* ./configure (build configuration)
* make  

This is the default in any build at ecd group. Do not deviate from it. These scripts are also   
the simplest form of documentation. They tell you exactly what is done.

## Organization and principles
<project_chip> directory is the top-level of the chip compilation. 
It has the following submodules

* environment_setup
  Contains set of scripts that build a analog design environment
  Provides set of scripts that can be used to pagkage analog macros
  for distribution, and to read packaged macros to virtuoso
  environment

* innovus_18.1
  Submodule for the digital flow

* macro-release-repo:
  Submodule for analog macro packages and related information
  information Intention is to
  isolate the macro information from the chisel generators.
  Should also provide possible gds,lib,lef files, but restructuring
  process is ongoing.
  THIS IS THE SOURCE OF ANALOG INFORMATION
  as Chisel needs mainly the verilog module.

Make recipes 
............
Make recipes are documented with make help, not here, but the structure is roughly this
(in order of execution)
* create_virtuoso_env
  Creates the virtuoso environment for verification and 
  macro merging

* import_analog
  Imports analog macros to that environment

* implement_digital
  Runs the digital flow in innovus_18.1

* import_digital_<blockname>
  Imports the top level verilog and gds from the digital flow
  to virtuoso. Macro layouts and schematics are already there. 
  This provides a very convenient environment for manual fixes. 
                       
* lvs
  Runs the lvs, to fix the DRC-errors, do it in virtuoso 
  with make 'debug_virtuoso'

* dummyfill
  If lvs is clean do the dummyfill

* lvs_dummyfill
  LVS the result

* drc
  Run the drc for the dummyfilled design

* notify
  Send message to Slack

* debug_virtuoso
  launch virtuoso in the build directory

* clean
  Cleans the run

Dos and Donts
.............
* Do not make decisions affecting others without asking others
* Buses are little endians
* On design tasks, communicate through issues.
* If you make a change that affect LVS, the one running that lvs is you.
* LVS macro before releasing it. It should be clean. If it is not, at least indicate it in commit message. 
* By default, block all layers over whole macro area. 
* Make proper pins with proper labeling. Pin defines the shape to which other connect. 
* Release your scripts so that others can fix them.

Chip flow
---------

```./init_submodules
source ./sourceme.sh and follow the instructions
./configure && make```

Your chip should be ready and checked in
`./virtuoso_\<ver\>_\<tech\>_\<topcell\>__\<date\>/\<topcell\>_dummyfill/\<topcell\>_finish.gds`



Virtuso working environment
...........................

Macro releases
..............

Macros are released with ./shell/release_cell.sh script. Documentation to be added.

Macro guidelines
''''''''''''''''
These guidelines are absolute minimum to follow in order not to break the flow that uses them. 
This is important, as every iteration to correct these takes *hours* at worst.
You will be hated A LOT if you do not do as istructed below due to ignorance or carelessness. Mistakes are Ok, but try to avoid them as well as you can.
* No one cares about the details you think are important. Pin locations and the shapes ate the only thing that matters. 
  Provide these fast, and as accurately as possible, but do not wait to iterate them. Do not change them if not absolutely necessary.
* Pins at the boundary. Not inside boundary or outside the boundary but AT the boundary.
* Make proper pins with proper size and labeling. Pin defines the shape to which other connect. 
* Supply pins are exeption. Make supply pins large and visible, preferebly on some top metal layer using same layer for VDD and VSS
* Block all layers with blockage if you do not *intentionally* want to route on that layer in Innovus.
* If you make a change that affect LVS, the one running that lvs is you.
* LVS macro before releasing. LVS should be clean. If it is not, at least indicate it in commit message. 
* By default, block all layers over whole macro area. 

Dale flow
=========
I think this need the documentation of it's own

Project status and releases
===========================

Release schedule
----------------
TBD

**Roadmap for next release**

* TBD 


**A bit longer term roadmap**

* TBD


**Release 1.0**

TBD


