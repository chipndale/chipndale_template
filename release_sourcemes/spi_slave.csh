# This script assumes that you are in proper relative position relative to your macro release repo directory
set dir = `pwd`
set macro = "spi_slave"
set techlib = "<TECH_LIB>" 

./shell/release_cell.sh -c ${macro} -f -l ${macro} \
-r "analogLib basic $techlib \
    <ALL_REFERENCE_LIBARIES_HERE>
" -s ${macro}_WD -t ${techlib} \
&& cd ../macro_repository/${macro} \
&& git checkout master && mv $dir/${macro}.tbz2 ./ \
&& git add $(macro).tbz2 \
&& git commit -m"Release ${macro}" \
&& echo "OK to push? [y|N]" && set ans = n && set ans = $<

if ( $ans == y) then
    git push && cd .. && git checkout master && git add ${macro} && git commit -m"Release ${macro}" 
else
    echo "Ans is $ans"
    echo "Not pushing"
endif
set ans = n
echo "OK to push? [y|N]" && set ans = $<

if ($ans == y) then
    git push && cd .. && git add macro_repository && git commit -m"Release ${macro}"
else
    echo "Not pushing"
endif
set ans = n
echo "OK to push? [y|N]" && set ans = $<

if ($ans == y) then
    git push && cd $dir
else
    echo "Not pushing"
endif

unset dir
unset macro
unset ans
unset techlib

